﻿using MarsRover.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MarsRover.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            var plateau=new Plateau(5, 5);
            Rover rover1 = new Rover(1, 2, Orientation.NORTH, plateau);
            var rover1Commands="LMLMLMLMM";
            System.Console.WriteLine("Rover1's position is : ({0},{1},{2}) ",rover1.Position.X,rover1.Position.Y,rover1.Position.Orientation.Code);
            System.Console.WriteLine("Rover1 is exploring with commands : {0}", rover1Commands);
            rover1.Explore(rover1Commands);
            System.Console.WriteLine("Rover1's position is : ({0},{1},{2}) ",rover1.Position.X,rover1.Position.Y,rover1.Position.Orientation.Code);

            Rover rover2 = new Rover(3, 3, Orientation.EAST, plateau);
            var rover2Commands = "MMRMMRMRRM";
            System.Console.WriteLine("Rover2's position is : ({0},{1},{2}) ", rover2.Position.X, rover2.Position.Y, rover2.Position.Orientation.Code);
            System.Console.WriteLine("Rover2 is exploring with commands : {0}", rover2Commands);
            rover2.Explore(rover2Commands);
            System.Console.WriteLine("Rover2's position is : ({0},{1},{2}) ", rover2.Position.X, rover2.Position.Y, rover2.Position.Orientation.Code);
            System.Console.ReadLine();

        }
    }
}
