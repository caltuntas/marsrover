﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MarsRover.Core
{
    public abstract class Command
    {
        protected Rover rover;
        public Command(Rover rover)
        {
            this.rover = rover;
        }

        public abstract void Execute();

    }
    public class MovementCommand:Command
    {
        public MovementCommand(Rover rover):base(rover)
        {
        }

        public override void  Execute()
        {
            var newPosition = rover.Position.Move();
            if (newPosition.WithinLimitsOf(rover.Plateau))
            {
                rover.Position = newPosition;
            }
        }
    }

    public class RotationCommand:Command
    {
        private string directionCode;
        public RotationCommand(Rover rover,string directionCode):base(rover)
        {
            this.directionCode = directionCode;
        }

        public override void Execute()
        {
            var direction = Direction.Parse(directionCode);
            rover.Position = rover.Position.Rotate(direction);
        }
    }
}
