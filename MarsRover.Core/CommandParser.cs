﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MarsRover.Core
{
    public class CommandParser
    {
        private Rover rover;
        public CommandParser(Rover rover)
        {
            this.rover = rover;
        }
        public List<Command> Parse(string commandString)
        {
            var commands = new List<Command>();
            for (int i = 0; i < commandString.Length; i++)
            {
                var command = commandString[i].ToString();
                if (command == "M")
                {
                    commands.Add(new MovementCommand(rover));
                }
                else
                {
                    commands.Add(new RotationCommand(rover,command));
                }
            }
            return commands;
        }
    }
}
