﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MarsRover.Core
{
    public class Direction
    {
        public string Code { get; set; }
        public bool Clockwise { get; set; }
        public int OrientationConstant { get; set; }

        public static readonly Direction LEFT = new Direction { Code = "L", Clockwise = false,OrientationConstant=-1 };
        public static readonly Direction RIGHT = new Direction { Code = "R", Clockwise = true ,OrientationConstant=1};

        private static Dictionary<string, Direction> directions = new Dictionary<string, Direction> {{"L",LEFT},{"R",RIGHT} };

        public static Direction Parse(string direction)
        {
            if(directions.ContainsKey(direction))
                return directions[direction];
            return null;
        }
    }
}
