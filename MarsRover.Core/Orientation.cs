﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MarsRover.Core
{
    public class Orientation
    {
        public int Value{get;set;}
        public int OrdinateMovement{get;set;}
        public int AbscissaMovement{get;set;}
        public string Code { get; set; }

        public static Orientation NORTH=new Orientation{Value=0,AbscissaMovement=0,OrdinateMovement=1,Code="N"};
        public static Orientation EAST=new Orientation{Value=1,AbscissaMovement=1,OrdinateMovement=0,Code="E"};
        public static Orientation SOUTH=new Orientation{Value=2,AbscissaMovement=0,OrdinateMovement=-1,Code="S"};
        public static Orientation WEST=new Orientation{Value=3,AbscissaMovement=-1,OrdinateMovement=0,Code="W"};

        private static readonly List<Orientation> values = new List<Orientation> { NORTH,EAST,SOUTH,WEST};

        public static Orientation Parse(int val)
        {
            return values.FirstOrDefault(x=>x.Value==val);
        }
    }
}
