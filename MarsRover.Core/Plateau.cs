﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MarsRover.Core
{
    public class Plateau
    {
        public int MaxX {get;set;}
        public int MaxY {get;set;}

        public Plateau(int x, int y)
        {
            this.MaxX = x;
            this.MaxY = y;
        }
    }
}
