﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MarsRover.Core
{
    public class Position
    {
        public int X{get;private set;}
        public int Y{get;private set;}
        public Orientation Orientation {get;set;}

        public Position(int x, int y, Orientation direction)
        {
            X = x;
            Y = y;
            Orientation = direction;
        }

        private static int Mod(int x)
        {
            int r = x % 4;
            return r < 0 ? r + 4 : r;
        }


        public Position Move()
        {
            var newX = X + Orientation.AbscissaMovement;
            var newY = Y + Orientation.OrdinateMovement;
            return new Position(newX, newY, Orientation);
        }

        public Position Rotate(Direction direction)
        {
            if (direction == null)
                return this;
            int directionValue = Mod(Orientation.Value + direction.OrientationConstant);
            var newOrientation=Orientation.Parse(directionValue);
            return new Position(X, Y, newOrientation);
        }

        public bool WithinLimitsOf(Plateau plateau)
        {
            return X>=0 && Y>=0 && X <= plateau.MaxX && Y <= plateau.MaxY;
        }
    }
}
