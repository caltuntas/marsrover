﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MarsRover.Core
{
    public class Rover
    {
        public Plateau Plateau { get; set; }
        public Position Position { get; set; }
        private CommandParser commandParser;

        public Rover(int x, int y, Orientation compass,Plateau plateau)
        {
            if (x < 0 || y < 0)
                throw new ArgumentOutOfRangeException("X or Y coordinates cannot be less than zero");
            this.Position = new Position(x, y, compass);
            this.Plateau = plateau;
            this.commandParser = new CommandParser(this);
        }


        public void Explore(string commandString)
        {
            var commands = commandParser.Parse(commandString);
            foreach (var command in commands)
            {
                command.Execute();
            }
        }
    }
}
