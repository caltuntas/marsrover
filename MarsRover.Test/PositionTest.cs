﻿using MarsRover.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MarsRover.Test
{
    [TestClass]
    public class PositionTest
    {
        [TestMethod]
        public void Increment()
        {
            var position = new Position(1, 1, Orientation.NORTH);
            position =position.Rotate(Direction.RIGHT);
            Assert.AreEqual(Orientation.EAST, position.Orientation);
        }


        [TestMethod]
        public void Increment_West()
        {
            var position = new Position(1, 1, Orientation.WEST);
            position =position.Rotate(Direction.RIGHT);
            Assert.AreEqual(Orientation.NORTH, position.Orientation);
        }


        [TestMethod]
        public void Increment_East()
        {
            var position = new Position(1, 1, Orientation.EAST);
            position =position =position.Rotate(Direction.RIGHT);
            Assert.AreEqual(Orientation.SOUTH, position.Orientation);
        }


        [TestMethod]
        public void Decrement()
        {
            var position = new Position(1, 1, Orientation.EAST);
            position=position.Rotate(Direction.LEFT);
            Assert.AreEqual(Orientation.NORTH, position.Orientation);
        }


        [TestMethod]
        public void Decrement_North()
        {
            var position = new Position(1, 1, Orientation.NORTH);
            position =position.Rotate(Direction.LEFT);
            Assert.AreEqual(Orientation.WEST, position.Orientation);
        }

        [TestMethod]
        public void Decrement_South()
        {
            var position = new Position(1, 1, Orientation.SOUTH);
            position =position.Rotate(Direction.LEFT);
            Assert.AreEqual(Orientation.EAST, position.Orientation);
        }
    }
}
