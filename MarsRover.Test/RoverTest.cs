﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MarsRover.Core;

namespace MarsRover.Test
{
    [TestClass]
    public class RoverTest
    {
        private Rover rover;

        [TestInitialize]
        public void SetUp()
        {
            var plateau = new Plateau(5,5);
            rover = new Rover(1, 2, Orientation.NORTH,plateau);
        }

        [TestMethod]
        public void Create_Rover_With_Position()
        {
            var position = rover.Position;
            Assert.AreEqual(1, position.X);
            Assert.AreEqual(2, position.Y);
            Assert.AreEqual(Orientation.NORTH, position.Orientation);
        }

        [TestMethod]
        public void Move_Rover()
        {
            rover.Explore("M");
            var position =rover.Position;
            Assert.AreEqual(1, position.X);
            Assert.AreEqual(3, position.Y);
            Assert.AreEqual(Orientation.NORTH, position.Orientation);
        }

        [TestMethod]
        public void Turn_Left_And_Move()
        {
            rover.Explore("L");
            rover.Explore("M");
            var position =rover.Position;
            Assert.AreEqual(0, position.X);
            Assert.AreEqual(2, position.Y);
            Assert.AreEqual(Orientation.WEST, position.Orientation);
        }


        [TestMethod]
        public void Turn_Right_And_Move()
        {
            rover.Explore("R");
            rover.Explore("M");
            var position =rover.Position;
            Assert.AreEqual(2, position.X);
            Assert.AreEqual(2, position.Y);
            Assert.AreEqual(Orientation.EAST, position.Orientation);
        }


        [TestMethod]
        public void Turn_Right_Twice_And_Move()
        {
            rover.Explore("R");
            rover.Explore("R");
            rover.Explore("M");
            var position =rover.Position;
            Assert.AreEqual(1, position.X);
            Assert.AreEqual(1, position.Y);
            Assert.AreEqual(Orientation.SOUTH, position.Orientation);
        }


        [TestMethod]
        public void Turn_Right_Multiple_Times()
        {
            rover.Explore("R");
            rover.Explore("R");
            rover.Explore("R");
            var position =rover.Position;
            Assert.AreEqual(1, position.X);
            Assert.AreEqual(2, position.Y);
            Assert.AreEqual(Orientation.WEST, position.Orientation);
        }

        [TestMethod]
        public void Invalid_Command_Should_Do_Nothing()
        {
            rover.Explore("K");
            var position = rover.Position;
            Assert.AreEqual(1, position.X);
            Assert.AreEqual(2, position.Y);
            Assert.AreEqual(Orientation.NORTH, position.Orientation);

        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void Create_Rover_Invalid_Position()
        {
            var rover = new Rover(-1, -1, Orientation.NORTH,new Plateau(5,5));
        }

        [TestMethod]
        public void Multiple_Commands_At_The_Same_Time()
        {
            rover.Explore("RRR");
            var position = rover.Position;
            Assert.AreEqual(1, position.X);
            Assert.AreEqual(2, position.Y);
            Assert.AreEqual(Orientation.WEST, position.Orientation);
        }

        [TestMethod]
        public void Move_Outside_Of_Plateau_X_Axis()
        {
            rover = new Rover(4, 1, Orientation.EAST, new Plateau(5, 5));
            rover.Explore("MMM");
            Assert.AreEqual(5, rover.Position.X);
            Assert.AreEqual(1, rover.Position.Y);
        }


        [TestMethod]
        public void Move_Outside_Of_Plateau_Y_Axis()
        {
            rover = new Rover(4, 1, Orientation.SOUTH, new Plateau(5, 5));
            rover.Explore("MMM");
            Assert.AreEqual(4, rover.Position.X);
            Assert.AreEqual(0, rover.Position.Y);

        }


        [TestMethod]
        public void Final_Test_For_Rover1()
        {
            rover = new Rover(1, 2, Orientation.NORTH, new Plateau(5, 5));
            rover.Explore("LMLMLMLMM");
            Assert.AreEqual(1, rover.Position.X);
            Assert.AreEqual(3, rover.Position.Y);
            Assert.AreEqual(Orientation.NORTH, rover.Position.Orientation);
        }

        [TestMethod]
        public void Final_Test_For_Rover2()
        {
            rover = new Rover(3, 3, Orientation.EAST, new Plateau(5, 5));
            rover.Explore("MMRMMRMRRM");
            Assert.AreEqual(5, rover.Position.X);
            Assert.AreEqual(1, rover.Position.Y);
            Assert.AreEqual(Orientation.EAST, rover.Position.Orientation);
        }

        [TestMethod]
        public void Command_String_Contains_Invalid_Commands()
        {
            rover = new Rover(3, 3, Orientation.EAST, new Plateau(5, 5));
            rover.Explore("MMRMMKRMRRM");
            Assert.AreEqual(5, rover.Position.X);
            Assert.AreEqual(1, rover.Position.Y);
            Assert.AreEqual(Orientation.EAST, rover.Position.Orientation);
        }


    }
}
